#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

require 5.005;

use Getopt::Long;
use FreePWING::FPWUtils::Word2;
use FreePWING::FPWUtils::KeyWord;
use FreePWING::FPWUtils::Text;
use FreePWING::FPWUtils::Menu;
use FreePWING::FPWUtils::Copyright;
use FreePWING::FPWUtils::Heading;
use FreePWING::FPWUtils::FPWUtils;

#
# コマンド行を解析する。
#
Getopt::Long::Configure("pass_through");
if (!GetOptions('workdir=s' => \$work_directory)) {
    exit 1;
}

#
# fpwutils を初期化する。
#
initialize_fpwutils();

#
# これから出力するファイルがすでにあれば、削除する。
#
unlink($text_file_name);
unlink($text_ref_file_name);
unlink($text_tag_file_name);
unlink($menu_file_name);
unlink($menu_ref_file_name);
unlink($menu_tag_file_name);
unlink($copyright_file_name);
unlink($copyright_ref_file_name);
unlink($copyright_tag_file_name);
unlink($heading_file_name);
unlink($word_file_name);
unlink($endword_file_name);
unlink($keyword_file_name);

#
# 各出力ファイルを開く。
#
sub initialize_fpwparser {
    my %arg = @_;

    #
    # 本文
    #
    if (defined($arg{'text'})) {
	${$arg{'text'}} = FreePWING::FPWUtils::Text->new();
        if (!${$arg{'text'}}->open()) {
	    die "$0: " . ${$arg{'text'}}->error_message() . "\n";
	}
    }

    #
    # メニュー
    #
    if (defined($arg{'menu'})) {
	${$arg{'menu'}} = FreePWING::FPWUtils::Menu->new();
        if (!${$arg{'menu'}}->open()) {
	    die "$0: " . ${$arg{'menu'}}->error_message() . "\n";
	}
    }

    #
    # 著作権表示
    #
    if (defined($arg{'copyright'})) {
	${$arg{'copyright'}} = FreePWING::FPWUtils::Copyright->new();
        if (!${$arg{'copyright'}}->open()) {
	    die "$0: " . ${arg{'copyright'}}->error_message() . "\n";
	}
    }

    #
    # 見出し
    #
    if (defined($arg{'heading'})) {
	${$arg{'heading'}} = FreePWING::FPWUtils::Heading->new();
	if (!${$arg{'heading'}}->open()) {
	    die "$0: " . ${$arg{'heading'}}->error_message() . "\n";
	}
    }

    #
    # 単語一覧
    #
    if (defined($arg{'word2'})) {
	${$arg{'word2'}} = FreePWING::FPWUtils::Word2->new();
	if (!${$arg{'word2'}}->open()) {
	    die "$0: " . ${$arg{'word2'}}->error_message() . "\n";
	}
    }

    #
    # 条件検索単語一覧
    #
    if (defined($arg{'keyword'})) {
	${$arg{'keyword'}} = FreePWING::FPWUtils::KeyWord->new();
	if (!${$arg{'keyword'}}->open()) {
	    die "$0: " . ${$arg{'keyword'}}->error_message() . "\n";
	}
    }
}

#
# 各出力ファイルを閉じる。
#
sub finalize_fpwparser {
    my %arg = @_;

    #
    # 本文
    #
    if (defined($arg{'text'}) && !${$arg{'text'}}->close()) {
	die "$0: " . ${$arg{'text'}}->error_message() . "\n";
    }

    #
    # メニュー
    #
    if (defined($arg{'menu'}) && !${$arg{'menu'}}->close()) {
	die "$0: " . ${$arg{'menu'}}->error_message() . "\n";
    }

    #
    # 著作権表示
    #
    if (defined($arg{'copyright'}) && !${$arg{'copyright'}}->close()) {
	die "$0: " . ${$arg{'copyright'}}->error_message() . "\n";
    }

    #
    # 見出し
    #
    if (defined($arg{'heading'}) && !${$arg{'heading'}}->close()) {
	die "$0: " . ${$arg{'heading'}}->error_message() . "\n";
    }

    #
    # 単語一覧
    #
    if (defined($arg{'word2'})) {
	if (!${$arg{'word2'}}->close()) {
	    die "$0: " . ${$arg{'word2'}}->error_message() . "\n";
        }
    }

    #
    # 条件検索単語一覧
    #
    if (defined($arg{'keyword'})) {
	if (!${$arg{'keyword'}}->close()) {
	    die "$0: " . ${$arg{'keyword'}}->error_message() . "\n";
        }
    }
}

#
# fpwutils の後始末をする。
#
finalize_fpwutils();

1;
