#                                                         -*- Perl -*-
# Copyright (c) 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 前方一致単語クラス (FreePWING::Word)、後方一致単語クラス
# (FreePWING::EndWord) を同時に操作するクラス
#
package FreePWING::FPWUtils::Word2;

require 5.005;
use English;
use FreePWING::Word2;
use FreePWING::FPWUtils::FPWUtils;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(FreePWING::Word2);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    return $type->SUPER::new();
}

#
# 書式:
#	open()
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	書き込み用の見出しファイルを開く。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub open {
    my $self = shift;
    
    return $self->SUPER::open($word_file_name, $endword_file_name);
}

#
# 書式:
#	add_entry(word)
#           word
#		単語
#           heading_position
#               見出しの位置
#           text_position
#               本文の位置
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	単語ファイルに単語を一つ追加する。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
# 備考:
#	FreePWING 1.1 までは、このメソッドの呼び出し方は
#	   add_entry(word, heading_position, heading_file_name,
#	           text_position, text_file_name)
#	だったので、そのための互換性を残してある。
#
sub add_entry {
    my $self = shift;
    my ($word, $heading_position, $text_position) = @ARG;

    if (@ARG == 5) {
	$heading_position = $ARG[1];
	$text_position = $ARG[3];
    }

    return $self->SUPER::add_entry($word,
				   $heading_position, $heading_file_name,
				   $text_position, $text_file_name);
}

1;
