#                                                         -*- Perl -*-
# Copyright (c) 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 本文中の著作権表示を生成するためのクラス。
#
package FreePWING::FPWUtils::Copyright;

require 5.005;
use English;
use FreePWING::Text;
use FreePWING::FPWUtils::FPWUtils;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(FreePWING::Text);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    return $type->SUPER::new();
}

#
# 書式:
#	open()
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	書き込み用の見出しファイルを開く。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub open {
    my $self = shift;
    
    if (!$self->SUPER::open($copyright_file_name, $copyright_ref_file_name,
			    $copyright_tag_file_name)) {
	return 0;
    }

    #
    # 外字の定義ファイルを読み込む。
    #
    if (-f $half_char_name_file_name
	&& !$self->set_half_user_characters_in_file($half_char_name_file_name)) {
        return 0;
    }
    if (-f $full_char_name_file_name
        && !$self->set_full_user_characters_in_file($full_char_name_file_name)) {
        return 0;
    }

    return 1;
}

1;
