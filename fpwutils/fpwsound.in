#! @PERL@
#                                                         -*- Perl -*-
# Copyright (c) 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

require 5.005;

use English;
use FreePWING::Sound;
use FreePWING::FPWUtils::FPWUtils;
use Getopt::Long;

#
# コマンド行を解析する。
#
if (!GetOptions('workdir=s' => \$work_directory)) {
    exit 1;
}

#
# fpwutils を初期化する。
#
initialize_fpwutils();

#
# これから出力するファイルがすでにあれば、削除する。
#
unlink($sound_file_name);
unlink($sound_tag_file_name);
unlink($sound_fmt_file_name);

$sound = FreePWING::Sound->new();

#
# 生成側ファイルを開く。
#
if (!$sound->open($sound_file_name,
		  $sound_tag_file_name,
		  $sound_fmt_file_name)) {
    die "$PROGRAM_NAME: " . $sound->error_message() . "\n";
}

foreach $file_name (@ARGV) {

    #
    # 音声の一覧を定義したファイルを開く。
    #
    $handle = FileHandle->new();
    if (!$handle->open($file_name, 'r')) {
	die "$PROGRAM_NAME: failed to open the file, $ERRNO: $file_name\n";
    }

    for (;;) {
	#
	# 一行読み込む。
	#
	$line = $handle->getline();
	if (!defined($line)) {
	    last;
	}
	$line =~ s/\r?\n?$//;

	#
	# 空行とコメント行 (`#' が先頭に来ている行) は無視する。
	#
	next if ($line =~ /^\#/ || $line =~ /^$/);

	#
	# 読み込んだ行を引数に分解する。
	# ちょうど 2 個 (ファイル名前とタグ名) ないとエラー。
	#
	@line_arguments = split(/[ \t]+/, $line);
	if (@line_arguments != 2) {
	    die "$PROGRAM_NAME: malformed line, at line $NR in $file_name\n";
	}

	#
	# 読み込んだ行に記された音声データを追加する。
	#
	if (!$sound->add_data(@line_arguments)) {
	    die "$PROGRAM_NAME: " . $sound->error_message() . "\n";
	}
    }

    #
    # 音声の一覧を記述したファイルを開く。
    #
    $handle->close();
}

#
# 音声の生成側ファイルを閉じる。
# 
if (!$sound->close()) {
    die "$PROGRAM_NAME: " . $sound->error_message() . "\n";
}
    
#
# fpwutils の後始末をする。
#
finalize_fpwutils();

exit 0;
