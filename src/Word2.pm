#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 前方一致単語クラス (FreePWING::Word)、後方一致単語クラス
# (FreePWING::EndWord) を同時に操作するクラス
#
package FreePWING::Word2;

require 5.005;
require Exporter;
use English;
use FreePWING::Word;
use FreePWING::EndWord;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(Exporter);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    my $new = {
	# 前方一致単語
	'word' => FreePWING::Word->new(),

	# 後方一致単語
	'endword' => FreePWING::EndWord->new(),

	# 前方一致単語ファイル名
	'word_file_name' => '',

	# 後方一致単語ファイル名
	'endword_file_name' => '',

	# これまでに書き込んだエントリ数
	'entry_count' => 0,

	# エラーメッセージ
	'error_message' => '',
    };
    return bless($new, $type);
}

#
# 書式:
#	open(word_file_name, endword_file_name)
#           word_file_name
#		開く前方一致単語ファイルの名前。
#           endword_file_name
#		開く後方一致単語ファイルの名前。
# メソッドの区分:
# 	public インスタンスメソッド
# 説明:
# 	書き込み用に単語ファイルを開く。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub open {
    my $self = shift;
    my ($word_file_name, $endword_file_name) = @ARG;

    #
    # 単語ファイルを開く。
    #
    $self->{'word_file_name'} = $word_file_name;
    $self->{'endword_file_name'} = $endword_file_name;
    if (!$self->{'word'}->open($word_file_name)) {
	$self->{'error_message'} = $self->{'word'}->error_message();
	return 0;
    }
    if (!$self->{'endword'}->open($endword_file_name)) {
	$self->{'error_message'} = $self->{'endword'}->error_message();
	$self->{'word'}->close();
	return 0;
    }
    return 1;
}

#
# 書式:
#	close()
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	単語ファイルを閉じる。開いていなければ、何もしない。
# 戻り値:
#	常に 1 を返す。
#
sub close {
    my $self = shift;

    $self->{'word'}->close();
    $self->{'endword'}->close();
    return 1;
}

#
# 書式:
#	add_entry(word, heading_position, heading_file_name,
#		  text_position, text_file_name)
#           word
#		単語
#           heading_position
# 		見出しの位置
#           heading_file_name
# 		見出しのファイル名
#           text_position
# 		本文の位置
#           text_file_name
# 		本文のファイル名
# メソッドの区分:
# 	public インスタンスメソッド。
# 説明:
# 	単語ファイルに単語を一つ追加する。
# 戻り値:
#	成功すれば 1 を返す。失敗すれば 0 を返す。
#
sub add_entry {
    my $self = shift;

    if (!$self->{'word'}->add_entry(@ARG)) {
	$self->{'error_message'} = $self->{'word'}->error_message();
	$self->{'endword'}->close();
	return 0;
    }
    if (!$self->{'endword'}->add_entry(@ARG)) {
	$self->{'error_message'} = $self->{'endword'}->error_message();
	$self->{'word'}->close();
	return 0;
    }
    $self->{'entry_count'}++;

    return 1;
}

######################################################################
# <インスタンス変数の値を返すメソッド群>
#
# 書式:
#	インスタンス変数名()
# メソッドの区分:
# 	public インスタンスメソッド。
# 戻り値:
#	インスタンス変数の値を返す。
#
sub word_file_name {
    my $self = shift;
    return $self->{'word_file_name'};
}

sub endword_file_name {
    my $self = shift;
    return $self->{'endword_file_name'};
}

sub entry_count {
    my $self = shift;
    return $self->{'entry_count'};
}

sub error_message {
    my $self = shift;
    return $self->{'error_message'};
}

1;
