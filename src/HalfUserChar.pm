#                                                         -*- Perl -*-
# Copyright (c) 2000  Motoyuki Kasahara
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 半角外字を納めたファイルを生成するクラス
#
package FreePWING::HalfUserChar;

require 5.005;
use FreePWING::BaseUserChar;
use strict;
use integer;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(FreePWING::BaseUserChar);

#
# 書式:
#	new()
# メソッドの区分:
# 	public クラスメソッド。
# 説明:
# 	新しいオブジェクトを作る。
# 戻り値:
# 	作成したオブジェクトへのリファレンスを返す。
#
sub new {
    my $type = shift;
    my $new = $type->SUPER::new();

    #
    # 外字 (高さ 16 〜 48 ドット) の幅を設定。
    #
    $new->{'bitmap_widths'}->[0] = 8;
    $new->{'bitmap_widths'}->[1] = 16;
    $new->{'bitmap_widths'}->[2] = 16;
    $new->{'bitmap_widths'}->[3] = 24;

    #
    # 外字 (高さ 16 〜 48 ドット) のビットマップの消費サイズを設定。
    #
    $new->{'bitmap_sizes'}->[0] = 16;
    $new->{'bitmap_sizes'}->[1] = 48;
    $new->{'bitmap_sizes'}->[2] = 60;
    $new->{'bitmap_sizes'}->[3] = 144;

    #
    # 外字 (高さ 16 〜 48 ドット) が 1024 バイト内に収まる個数を設定。
    #
    $new->{'bitmap_pad_cycles'}->[0] = 64;
    $new->{'bitmap_pad_cycles'}->[1] = 21;
    $new->{'bitmap_pad_cycles'}->[2] = 17;
    $new->{'bitmap_pad_cycles'}->[3] = 7;

    #
    # 外字 (高さ 16 〜 48 ドット) を書き込むときに、詰め物をして次の
    # ブロックに移る場合における詰め物のサイズを設定。
    #
    $new->{'bitmap_pad_lengths'}->[0] = 0;
    $new->{'bitmap_pad_lengths'}->[1] = 16;
    $new->{'bitmap_pad_lengths'}->[2] = 4;
    $new->{'bitmap_pad_lengths'}->[3] = 16;

    return $new;
}

1;
