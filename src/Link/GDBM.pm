#                                                         -*- Perl -*-
# Copyright (c) 1999, 2000  Motoyuki Kasahara
# Copyright (c) 2008  Kazuhiro Ito
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# 複数のファイルを連結するためのクラス。
#
package FreePWING::Link::GDBM;

require 5.005;
use English;
use FreePWING::Link::Hash;
use strict;
use integer;

use GDBM_File;
use Fcntl;

use vars qw(@ISA
	    @EXPORT
	    @EXPORT_OK);

@ISA = qw(FreePWING::Link::Hash);

sub open_internal {
    my $self = shift;
    my ($output_file_name) = @ARG;

    $self->{'tag_file_name'} = "$output_file_name.tag";
    tie(%{$self->{'tag_table'}}, 'GDBM_File', $self->{'tag_file_name'},
	O_RDWR | O_CREAT | O_TRUNC, 0644);
}

sub close_internal2 {
    my $self = shift;

    untie($self->{'tag_table'});
    unlink($self->{'tag_file_name'});
}

1;
